# UserDividends SDK Integration Example

## Step 1 - Add the SDK to your project

Depending on how your project is setup you need to add the following line in one of two places:

```
        maven { url 'https://android-sdk.userdividends.com' }
```

**settings.gradle**

```
    dependencyResolutionManagement {
        repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
        repositories {
            google()
            mavenCentral()
            
            maven { url 'https://android-sdk.userdividends.com' }
            
        }
    }
```

**build.gradle** (project level)

```
repositories {
        google()
        mavenCentral()
        
        maven { url 'https://android-sdk.userdividends.com' }
        
    }
```

## Step 2 - Add app level build.gradle dependency

```
    implementation "com.userdividends.sdk:rev-share-sdk:1.0.8"
```

## Step 3 - Initialize the SDK

Either in your application onCreate or your main activity onCreate function you need to initilize the UserDividends SDK before it's used. Initialization requires your app id, if you'r testing simply use the 'Test-App-'

```
        UserDividends.init(this,"Test-App-Id")
```

## Step 4 - Report Revenue

Any revenue events that happen inside your app should be reported similar to the following:

```
    UserDividends.reportRevenue(
        context = this,
        amount  = 350
    )

```

### Notes

- The SDK will use the currency for the device unless you override it and pass it in as a parameter during revenue reporting.
- Revenue is reported as whole numbers in the smallest unit of the currency.  For example, in USD, to report $2.50 you would pass in 250.
- A network connection is not required for any part of the SDK to work.  If you report revenue and a connection is not available, the SDK will manage sending that report when it's feasibly possible.

### Testing against a campaign

Testing against a real campaign requires the campaigns click through URL which contains the CTID for campaign tracking. You can test this with either a real click through URL or a dummy one.  The process works the same for both.
To test perform the following steps:

1. Uninstall your app from the device
2. On the Android device navigate to a modified URL that contains your apps package name and UserDividends click through URL.  This URL will take you to Google Play on the device, 
   note that it doesn't matter if your app is physically in Google Play or not.  All that matters is that the app id matches yours in the URL.
    a. https://play.google.com/store/apps/details?<your-app-package-name>&referrer=udctid%3Ddummy-ctid-test
    b. example:  https://play.google.com/store/apps/details?id=com.userdividends.example.integration&referrer=udctid%3Drichard-test
    c. In the test above we assume that the apps package name is:  com.userdividends.example.integration, replace this with your apps package name if not testing this app
    d. You will notice that the dummy ctid is attached to the end of the url, you can change everything after the %3D
3. Once you click on the link and you are sitting in Google Play, install your app from Android Studio.
4. Now when you run an app with UserDividends integrated you can report revenue against a dummy campaign and track the fact that the user came from a UserDividends partner.
5. You can view the fact that the campaign id is picked up by clicking on the "Get Info" button in this test app