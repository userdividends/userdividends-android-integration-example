package com.userdividends.example.integration

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.userdividends.UserDividends
import com.userdividends.example.integration.databinding.ActivityMainBinding

///////////////////////////
// CONSTANTS
///////////////////////////
const val APP_ID        = "Test-App-Id"

///////////////////////////
// MAIN CLASS
///////////////////////////
class MainActivity : AppCompatActivity() {

    /////////////////////////////////////
    // PRIVATE VARS
    /////////////////////////////////////
    private lateinit var binding : ActivityMainBinding

    /////////////////////////////////////
    // PUBLIC FUNCTIONS
    /////////////////////////////////////
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // attach listeners
        binding.reportRevenue.setOnClickListener { reportRevenue() }
        binding.getInfo.setOnClickListener { printInfo() }
        binding.enableDeveloperMode.setOnClickListener() { enableTestMode() }

        // initialize UserDividends SDK
        initializeUserDividends()
    }

    /////////////////////////////////////
    // PRIVATE FUNCTIONS
    /////////////////////////////////////
    private fun initializeUserDividends() {
        UserDividends.init(this, APP_ID)
        binding.output.text.append("UserDividends SDK initialized\n")
    }

    private fun reportRevenue() {
        if(UserDividends.campaignId.isEmpty()) {
            binding.output.text.append("No campaign to track revenue against\n")
        }else {

            try {
                UserDividends.reportRevenue(
                    context = this,
                    amount = Integer.parseInt(binding.amount.text.toString())
                )
                binding.output.text.append("Reported ${binding.amount.text} cents USD in revenue\n")
            }catch(e : Exception) {
                binding.output.text.append("Please enter a whole number in cents\n")
            }
        }
    }

    private fun printInfo() {
        binding.output.text.append("Found campaign id: ${UserDividends.campaignId}\n")
        binding.output.text.append("Device generated id: ${UserDividends.deviceGeneratedId}\n")
    }

    private fun enableTestMode() {
            UserDividends.init(
                context         = this,
                appId           = APP_ID,
                isDevelopment   = true)

            binding.output.text.append("Development mode enabled. To disable you must restart the app. Click get info button...\n")
    }
}